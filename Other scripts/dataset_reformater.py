import pandas as pd


temp_table1 = pd.read_csv("table1.csv", header=0)
table2 = pd.read_csv("table2.csv", header=0)
# Create table1 instance
table1 = pd.DataFrame(columns=["Name", "Value", "Position"])

# Remove column 1 from table1, because has not information
dropped_first_column = temp_table1.drop(['Column 1'], axis=1)
# Split based on comma
splitted_by_comma = str(dropped_first_column['Column 2'].to_string()).split(',')
# Split based on new line character
splitted_by_newline = [i.split('\n') for i in splitted_by_comma]
for line in splitted_by_newline[0]:
    temp = line.split('\\n')
    # First remove the number
    name = ''.join([i for i in temp[0] if not i.isdigit()])
    # Remove whitespaces in the front
    name = name.lstrip()
    # name, temp[2], temp[4]
    table1 = table1.append({'Name':     name,
                            'Value':    temp[2],
                            'Position': temp[4]}, ignore_index=True)

final_table = pd.concat([table1, table2], axis=1, sort=True)
# Export it to csv
final_table.to_csv("final_table.csv", encoding="utf-8")
