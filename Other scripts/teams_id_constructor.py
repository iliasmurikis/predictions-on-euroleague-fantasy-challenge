import pandas as pd

pd.set_option('display.max_columns', None)

df = pd.read_csv("../Data/main_dataset.csv")
df = df.assign(id=(df['OpponentTeam']).astype('category').cat.codes)
print(df[df['PlayerID'] == 337])
