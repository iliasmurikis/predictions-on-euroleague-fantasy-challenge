import glob
import cv2
import matplotlib.pyplot as plt


""" Import images """
images = [cv2.cvtColor(cv2.imread(file), cv2.COLOR_BGR2RGB) for file in sorted(glob.glob(
    "../Plots/Neural Plots/FantasyPointsPrediction/*.png"))]

""" Create three subplots """
columns = 3
rows = 1
fig1 = plt.figure(figsize=(16, 5))
for i in range(1, 4):
    fig1.add_subplot(rows, columns, i)
    plt.imshow(images[i-1])
fig1.suptitle('MLP', fontsize=24)
plt.savefig('../Plots/Neural Plots/NEW/mlp.png')

fig2 = plt.figure(figsize=(16, 5))
for i in range(1, 4):
    fig2.add_subplot(rows, columns, i)
    plt.imshow(images[i+3-1])
fig2.suptitle('Simple RNN', fontsize=24)
plt.savefig('../Plots/Neural Plots/NEW/simplernn.png')

fig3 = plt.figure(figsize=(16, 5))
for i in range(1, 4):
    fig3.add_subplot(rows, columns, i)
    plt.imshow(images[i+6-1])
fig3.suptitle('LSTM', fontsize=24)
plt.savefig('../Plots/Neural Plots/NEW/lstm.png')

""" Combine to one """
image1 = cv2.cvtColor(cv2.imread('../Plots/Neural Plots/FantasyPointsPrediction/mlp.png'), cv2.COLOR_BGR2RGB)
image2 = cv2.cvtColor(cv2.imread('../Plots/Neural Plots/FantasyPointsPrediction/simplernn.png'), cv2.COLOR_BGR2RGB)
image3 = cv2.cvtColor(cv2.imread('../Plots/Neural Plots/FantasyPointsPrediction/lstm.png'), cv2.COLOR_BGR2RGB)
columns = 1
rows = 3

fig = plt.figure(figsize=(32, 32))
fig.add_subplot(rows, columns, 1)
plt.imshow(image1)
plt.axis('off')
fig.add_subplot(rows, columns, 2)
plt.imshow(image2)
plt.axis('off')
fig.add_subplot(rows, columns, 3)
plt.imshow(image3)
plt.axis('off')
plt.savefig('../Plots/Neural Plots/NEW/combined.png', bbox_inches='tight')
