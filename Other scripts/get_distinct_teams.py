import pandas as pd


dataset = pd.read_csv("~/Documents/Scholarship/Data/realgm_data_teams_stats.csv", index_col=None)
unique_teams = dataset['Team'].unique()

# print(dataset.head())
for team in unique_teams:
    print(team)
