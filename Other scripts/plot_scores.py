import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

df = pd.read_csv('../Data/results_TeamSuggestion.csv')
# df = df.set_index('score')
# print(df)

color_list = ['blue', 'orange', 'green']
names = list(df.columns.values)[1:]
exclude_list = []

""" Plotting everything """
plt.figure(figsize=(8, 5))
for i in range(len(names)):
    sns.lineplot(data=df[str(names[i])].astype('float64'),
                 label=names[i], color=color_list[i])
plt.xticks(range(len(df['score'])), df['score'].to_list())
plt.legend()
plt.title('Plot of scores')
plt.savefig('../Plots/Neural Plots/TeamSuggestion/scores.png', bbox_inches='tight')
