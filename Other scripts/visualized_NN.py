import os
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import plot_model


path = "../Models/ShotsPrediction/approachLSTM.hdf5"
if os.path.exists(path):
    model = load_model(path)

    plot_model(model, to_file='../Plots/Neural Plots/ShotsPrediction/model_plot(LSTM).png',
               show_shapes=True, show_layer_names=True)
