import os
import sys
import numpy as np
import pandas as pd
from joblib import load
from tensorflow.keras.models import load_model


def LoadBestModel():
    path = "../Models/TeamSuggestion/approachMLP.hdf5"
    if os.path.exists(path):
        model = load_model(path)
        return model
    else:
        print("Warning: Best model does not exist!")
        return 0


def FindNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def Predict(record):
    """ Normalise data inside """
    temp_record = record.reshape(1, -1)
    if not os.path.exists('std_scaler(suggestion).bin'):
        print('Scaler does not exist!')
        print('Exiting...')
        sys.exit()
    else:
        sc_X = load('std_scaler(suggestion).bin')
        temp_record = sc_X.transform(temp_record)

    """ Load model """
    model = LoadBestModel()

    """ Reshaping """
    temp_record = temp_record.reshape((temp_record.shape[0], temp_record.shape[1]))

    """ Predict with the best saved model """
    y_pred = np.around(model.predict(temp_record)).astype(int)

    """ Match predictions with valid IDs """
    for i in range(len(y_pred)):
        y_pred[i] = FindNearest(array=players_IDs, value=y_pred[i])

    return y_pred[0][0]


""" Create a list with all players' IDs """
players_df = pd.read_csv('../Data/players.csv')
players_IDs = players_df['playerID'].to_numpy()

record = [-1] * 17
# For randomness change the initial budget
record[-1] = 10500000
record = np.asarray(record)

for i in range(8):
    """ Predict """
    y_pred = Predict(record)
    """ Update player's cost """
    record[i*2] = players_df[players_df['playerID'] == y_pred]['Cost'] * 500
    """ Update player's ID """
    record[i*2+1] = y_pred
    """ Update total money """
    record[-1] = record[-1] - record[i*2]
    # print(record)

sum = 0
for i in range(8):
    print(record[i*2+1], '-', players_df[players_df['playerID'] == record[i*2+1]]['PlayerName'].values[0],
          ':', record[i*2])
    sum += record[i*2]
print('Total team cost is:', sum)
