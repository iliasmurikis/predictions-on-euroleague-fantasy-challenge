import os
import sys
import time
import pandas as pd
import numpy as np
from math import floor, ceil
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from joblib import dump, load
import tensorflow as tf
from tensorflow.keras.callbacks import History, EarlyStopping, Callback
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import SimpleRNN, Dense, InputLayer


def FindNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def LoadBestModel():
    path = "../Models/TeamSuggestion/approachSimpleRNN.hdf5"
    if os.path.exists(path):
        model = load_model(path)
        return model
    else:
        print("Warning: Best model does not exist!")
        return 0


""" Design NN network """
def train_model(xtrsc_3d, ytrsc_3d):
    def make_reproducible_results_tf():
        """MAKE REPRODUCIBLE RESULTS"""
        sess = tf.compat.v1.keras.backend.get_session()
        tf.keras.backend.clear_session()
        sess.close()
        sess = tf.compat.v1.keras.backend.get_session()
        """4. Set `tensorflow` pseudo-random generator at a fixed value"""
        np.random.seed(1)
        tf.random.set_seed(9999)
        """5. Configure a new global `tensorflow` session"""
        if tf.test.is_gpu_available():
            print('Inside GPU clause - termination...')
            sys.exit()
        else:
            session_conf = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=0, inter_op_parallelism_threads=0)
            sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
            print(tf.test.gpu_device_name())
        tf.compat.v1.keras.backend.set_session(sess)


    class ClassTimeHistory(Callback):
        """Count processing time"""
        def on_train_begin(self, logs={}):
            self.times = []

        def on_epoch_begin(self, batch, logs={}):
            self.epoch_time_start = time.time()

        def on_epoch_end(self, batch, logs={}):
            self.times.append(time.time() - self.epoch_time_start)


    def plot_loss_training(history):
        plt.figure()
        plt.plot(history.history['loss'], label='train')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.plot(history.history['val_loss'], label='val')
        plt.legend()
        plt.show()


    def construct_nn(xtrsc):
        model = Sequential()
        model.add(InputLayer(input_shape=(xtrsc.shape[-2:])))
        model.add(SimpleRNN(25, activation='relu', dropout=0.5))
        model.add(Dense(50, activation='relu'))
        model.add(Dense(1, activation='linear'))

        return model


    def train_nn(model, xtrsc, ytrsc, callbacks_list):
        print(model.summary())
        model.compile(loss='mae', optimizer='adam')
        """ Fit the model """
        history = model.fit(x=xtrsc, y=ytrsc,
                            epochs=EPOCHS, batch_size=BATCH_SIZE,
                            validation_split=0.2,
                            callbacks=callbacks_list, verbose=1,
                            use_multiprocessing=True, workers=4)

        """ Calculate Training Time (sec) """
        print("Training time (sec):")
        times = time_callback.times
        print(sum(times))
        return history, model, times


    BATCH_SIZE = 1
    EPOCHS = 1000
    # Make reproducible results
    make_reproducible_results_tf()
    # Time
    time_callback = ClassTimeHistory()
    # Checkpoint
    # Early Stopping
    earlyStopping = EarlyStopping(monitor='val_loss', patience=50, verbose=2, mode='auto')
    # History
    history = History()
    # List all callbacks
    callbacks_list = [earlyStopping, time_callback, history]    # checkpoint,

    # If best model exists, read it and continue training
    model = LoadBestModel()
    if model == 0:
        # Create the NN model
        model = construct_nn(xtrsc_3d)

    # Train model
    history, model, times = train_nn(model, xtrsc_3d, ytrsc_3d, callbacks_list)

    plot_loss_training(model.history)
    # Export best_model as a unique name
    model.save("../Models/TeamSuggestion/approachSimpleRNN.hdf5")


# Packages settings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# Program begins...
df = pd.read_csv("../Data/team_suggestion_dataset2.csv")
df = df.drop(['Unnamed: 0'], axis=1)
# print(df.columns)
# print(df.shape)

""" Create a list with all players' IDs """
players_df = pd.read_csv('../Data/players.csv')
players_IDs = players_df['playerID'].to_numpy()

""" Convert dataframe to array """
data = df.to_numpy()

""" Extract to-be-predicted column (y) """
# column  which have the y, which we want to predict
y_column = df.columns.get_loc('y')
y = data[:, y_column]
x = np.delete(data, y_column, axis=1)
print("x shape:", x.shape)
print("y shape:", y.shape)

""" Normalise data inside [0, 1], using MinMax """
# Convert 3D array to 2D
# x = x.reshape((x.shape[0], x.shape[1]))
# Apply normalization
if not os.path.exists('std_scaler(suggestion).bin'):
    sc_X = StandardScaler()
    x = sc_X.fit_transform(x)
    dump(sc_X, 'std_scaler(suggestion).bin', compress=True)
else:
    sc_X = load('std_scaler(suggestion).bin')
    x = sc_X.transform(x)
x = x.reshape((x.shape[0], x.shape[1], 1))
print(x.shape)

""" Split into training and testing sets """
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=23)
print("x_train: ", x_train.shape)
print("y_train: ", y_train.shape)

""" Train the model """
train_model(x_train, y_train)

# Reformat x so to become 3-dimensional
x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], 1))
print("x_test: ", x_test.shape)
print("y_test: ", y_test.shape)

""" Load best model """
best_model = LoadBestModel()

""" Predict with the best saved model """
y_pred = np.around(best_model.predict(x_test)).astype(int)
print('y_pred shape:', y_pred.shape)

""" Match predictions with valid IDs """
for i in range(len(y_pred)):
    y_pred[i] = FindNearest(array=players_IDs, value=y_pred[i])

fig, ax = plt.subplots()
y = np.array(range(floor(np.min(y_pred)), ceil(np.max(y_pred) + 1)))
plt.plot(y, y, 'red')
ax.scatter(x=y_test[:, ], y=y_pred[:, ])
plt.xlabel('Actual Player\'s ID')
plt.ylabel('Predicted Player\'s ID')
plt.title('Visual comparison for actual vs predicted Player\'s ID')
plt.show()

""" Calculate Error """
deviations_series = pd.Series(data=(y_test[:, ] - y_pred[:, 0]))
sns.distplot(deviations_series, bins=40)
plt.title('Error of predicted Player\'s ID')
plt.xlabel('Error')
plt.ylabel('Frequency')
plt.show()

print('MSE:', mean_squared_error(y_test, y_pred))
print('RMSE:', mean_squared_error(y_test, y_pred, squared=False))
print('MAE:', mean_absolute_error(y_test, y_pred))
print('R^2:', r2_score(y_test, y_pred))
