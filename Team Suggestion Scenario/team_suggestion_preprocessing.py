import pandas as pd


def append_row(df, record):
    df = df.append(pd.Series(record, index=df.columns[:len(record)]), ignore_index=True)

    return df


# Personal settings
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 12)
pd.set_option('display.width', 1000)
pd.options.mode.chained_assignment = None

df = pd.read_csv('../Data/teams.csv', header=None)
players_df = pd.read_csv('../Data/players_for_teams.csv')

""" Dropping column that contains teams' owners names """
df = df.drop([0], axis=1)
# print(df.columns.values)

""" Add a new column that contains incremental numbers,
    to be used as new IDs                               """
players_df = players_df.reset_index()
# players_df.columns[0] = 'newID'
players_df['newPlayerID'] = players_df.index
# print(players_df.head(10))

""" Set column names """
columns_list = []
for i in range(1, 9):
    columns_list.append('player' + str(i))
df.columns = columns_list
# print(df.columns.values)

# Initiate cost columns and playerID columns
for i in range(1, 9):
    df['player' + str(i) + 'value'] = -1
    df['player' + str(i) + 'ID'] = -1
# print(df.head())

""" Update values into the previously created columns """
for i in range(1, 9):
    df['player' + str(i) + 'value'] = df['player' + str(i)].map(players_df.set_index('PlayerName')['Cost'])
# print(df.head())

""" Add a column for total team's cost """
df['total_cost'] = 0
for row_index, _ in df.iterrows():
    sum = 0
    for i in range(1, 9):
        sum += df['player' + str(i) + 'value'].iloc[row_index]
    # Update value
    df.at[row_index, 'total_cost'] = sum

""" Convert everything to IDs """
for i in range(1, 9):
    df['player' + str(i) + 'ID'] = df['player' + str(i)].map(players_df.set_index('PlayerName')['newPlayerID'])

""" Create a column for the prediction value """
df['y'] = -1

""" Drop columns with names """
temp_list = []
for i in range(1, 9):
    temp_list.append('player' + str(i))
df = df.drop(temp_list, axis=1)
print(df.head())

""" Create a column list in which there are all column names 
    except player names columns                              """
columns_list = list(df.columns.values)

""" Create a new empty dataframe with these column names """
dataset_df = pd.DataFrame(columns=columns_list)
pos = df.columns.get_loc('total_cost')
y_pos = df.columns.get_loc('y')
for row_index, row in df.iterrows():
    record = [-1] * len(row)
    cost = row.total_cost
    record[pos] = cost * 500
    record[y_pos] = row.player1ID
    # Insert into df
    dataset_df = append_row(df=dataset_df, record=record)
    for i in range(1, 8):
        id = df.iloc[row_index]['player' + str(i) + 'ID']
        value = df.iloc[row_index]['player' + str(i) + 'value'] * 500
        # Update record
        record[(i - 1) * 2]     = value
        record[(i - 1) * 2 + 1] = id
        record[pos] -= value
        record[y_pos] = row['player' + str(i + 1) + 'ID']
        # Insert into df
        dataset_df = append_row(df=dataset_df, record=record)

print(dataset_df.head(20))
print(dataset_df.shape)

# Export to csv...
dataset_df.to_csv('../Data/team_suggestion_dataset2.csv')
