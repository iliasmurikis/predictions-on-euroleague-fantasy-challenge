# Predictions Project for Euroleague Fantasy Challenge Prediction

### Description

This project is about three different prediction scenarios on a strategy game called "Euroleague 
Fantasy Challenge". <br/> 
All the prediction models are implemented with Neural Networks using the Tensorflow framework. <br/>

Within the project files are everything that is required to replicate all the experiments. 
Also, there are the trained models and the corresponding plots. <br/>

All three scenarios are useful to predict various metrics from inside the game and serve in favor 
of the player to do better choices on his/her gameplay. With details, in the first scenario, the 
predicted attribute is the Fantasy Points that a player is about to win in an upcoming match. In 
the second scenario the successful shots for each player of the game are predicted and in this way 
the winning team. And finally, the third scenario is about suggesting a team combination to a new 
player.

### Project structure

The structure of the project is separated into distinct folders. In the folder named "Scrappers"
there the code for data acquisition from the web. All the files that was downloaded or processed 
later are stored in the folder "Data".<br/>
The code for the Neural Networks for each of the three scenarios are in the folders called 
"Fantasy Points Prediction Scenario", "Team Suggestion Scenario" and "Winning Team Prediction 
Scenario".<br/>
In each folder there three scripts for the three different types of Neural Networks implemented, 
one Multilayer Perceptron, one Simple RNN and one LSTM. Also , in some implementations additional 
preprocessing is required, and so there might be additional scripts.<br/>
There are three more folders. The first folder called "Other scripts" contains other scripts that 
are needed for distinct tasks, such as plotting a Neural Network's architecture, or merging separate
plots into one. The second folder called "Models" contains all the trained models. And finally, the 
third called "Plots", contains all the plots that have created for visualizing training results or
model accuracy.
