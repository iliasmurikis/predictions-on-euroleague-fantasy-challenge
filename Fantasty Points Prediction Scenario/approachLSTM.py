import os
import sys
import time
import pandas as pd
import numpy as np
from math import floor, ceil
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import TimeSeriesSplit
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
import tensorflow as tf
from tensorflow.keras.callbacks import History, EarlyStopping, Callback
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, LSTM, Dropout


def LoadBestModel():
    path = "../Models/FantasyPointsPrediction/approachLSTM.hdf5"
    if os.path.exists(path):
        model = load_model(path)
        return model
    else:
        print("Warning: Best model does not exist!")
        return 0


""" Design NN network """
def train_model(xtrsc_3d, ytrsc_3d):
    def make_reproducible_results_tf():
        """MAKE REPRODUCIBLE RESULTS"""
        sess = tf.compat.v1.keras.backend.get_session()
        tf.keras.backend.clear_session()
        sess.close()
        sess = tf.compat.v1.keras.backend.get_session()
        """4. Set `tensorflow` pseudo-random generator at a fixed value"""
        np.random.seed(1)
        tf.random.set_seed(9999)
        """5. Configure a new global `tensorflow` session"""
        if tf.test.is_gpu_available():
            print('Inside GPU clause - termination...')
            sys.exit()
        else:
            session_conf = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=0, inter_op_parallelism_threads=0)
            sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
            print(tf.test.gpu_device_name())
        tf.compat.v1.keras.backend.set_session(sess)


    class ClassTimeHistory(Callback):
        """Count processing time"""
        def on_train_begin(self, logs={}):
            self.times = []

        def on_epoch_begin(self, batch, logs={}):
            self.epoch_time_start = time.time()

        def on_epoch_end(self, batch, logs={}):
            self.times.append(time.time() - self.epoch_time_start)


    def plot_loss_training(history):
        plt.figure()
        plt.plot(history.history['loss'], label='train')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.plot(history.history['val_loss'], label='val')
        plt.legend()
        plt.show()


    def construct_nn(xtrsc):
        model = Sequential()
        model.add(LSTM(25, activation='relu', input_shape=(xtrsc.shape[-2:])))
        model.add(Dropout(0.5))
        model.add(Dense(20, activation='relu'))
        model.add(Dense(1, activation='linear'))

        return model


    def train_nn(model, xtrsc, ytrsc, callbacks_list):
        print(model.summary())
        model.compile(loss='mse', optimizer='adam')
        """ Fit the model """
        history = model.fit(x=xtrsc, y=ytrsc,
                            epochs=EPOCHS, batch_size=BATCH_SIZE,
                            validation_split=0.2,
                            callbacks=callbacks_list, verbose=1,
                            use_multiprocessing=True, workers=4,
                            shuffle=False)
        """ Calculate Training Time (sec) """
        print("Training time (sec):")
        times = time_callback.times
        print(sum(times))
        return history, model, times


    EPOCHS = 1000
    BATCH_SIZE = 1
    # Make reproducible results
    make_reproducible_results_tf()
    # Time
    time_callback = ClassTimeHistory()
    # Early Stopping
    earlyStopping = EarlyStopping(monitor='val_loss', patience=50, verbose=2, mode='auto')
    # History
    history = History()
    # List all callbacks
    callbacks_list = [earlyStopping, time_callback, history]    # checkpoint,

    # If best model exists, read it and continue training
    model = LoadBestModel()
    if model == 0:
        # Create the NN model
        model = construct_nn(xtrsc_3d)
    # Train model
    history, model, times = train_nn(model, xtrsc_3d, ytrsc_3d, callbacks_list)

    plot_loss_training(model.history)
    # Export best_model as a unique name
    model.save("../Models/FantasyPointsPrediction/approachLSTM.hdf5")


# Packages settings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# Program begins...
df = pd.read_csv("../Data/main_dataset.csv")
# print(df.columns)
# print(df.shape)

# Order by PlayerID and then by RD
df = df.sort_values(by=['PlayerID', 'RD'])

# Replace non-numerical values with NaN
df = df.replace('-', np.NaN)
df = df.replace('DNP', np.NaN)
# Drop nan values or replace
df = df.dropna()
df.reset_index(drop=True, inplace=True)
# Drop unnecessary columns
df = df.drop(['RD', 'PlayerName', 'Venue'], axis=1)
# print(df.shape)
# print(df.columns.values)
# print(df.describe())
# print(df.head(20))

""" Create a correlation matrix of the available variables """
# corrMatrix = df.corr()
# sns.heatmap(corrMatrix, annot=True)
# plt.show()

""" Convert series to supervised problem """
STEP = 5
list_of_xs = []
list_of_ys = []
grouped = df.groupby('PlayerID')
# print(len(grouped))
# 269 players into the cleared dataset
for _, group in grouped:
    index = 0
    for global_index, row in group.iterrows():
        # Creating a local index, due to the mistaken global that pandas uses
        """ Checking if there are at least 2 previous matches,
            so with the current on to be STEP, stateful=False """
        if (index + STEP) > len(group) or len(group) < STEP:
            index = 0
            break

        """ Create steps """
        # Here, global_index is used because it was reset after the sorting
        steps = group.loc[global_index:global_index + STEP - 1].to_numpy()
        # print(steps)
        # print(steps.shape)
        list_of_xs.append(steps)

        """ Extract the to-be-predicted column """
        y = steps[STEP - 1, 5]
        # print(y)
        list_of_ys.append(y)

        # Move to the next position
        index += 1


# print(len(list_of_xs))

""" Convert lists into arrays """
x = np.zeros((len(list_of_xs), list_of_xs[0].shape[0], list_of_xs[0].shape[1]))

for i in range(len(list_of_xs)):
    x[i, :, :] = list_of_xs[i]
# print(x.shape)
# print(type(x))
y = np.asarray(list_of_ys)
# print(y)
# print(y.shape)

""" Set specific values equal to 0 in the first/un-played match """
for i in range(len(list_of_xs)):
    x[i, STEP - 1, 5:23] = 0
print("x shape:", x.shape)
print("y shape:", y.shape)

""" Normalise data inside [0, 1], using StandardScaler """
# Convert 3D array to 2D
players, matches, attrs = x.shape
x = x.reshape((players * matches, attrs))   # , order='C')
# Apply normalization
sc_X = StandardScaler()
x = sc_X.fit_transform(x)
# Convert 2D array to 3D
x = x.reshape((players, matches, attrs))

""" Split into training and testing sets """
tscv = TimeSeriesSplit(n_splits=2)
first_iteration = True
for train_index, test_index in tscv.split(x):
    """ Ignore first iteration """
    if first_iteration:
        first_iteration = False
        continue

    x_train, x_test = x[train_index], x[test_index]
    y_train, y_test = y[train_index], y[test_index]
    y_train = y_train.astype('float64')
    y_test = y_test.astype('float64')
    # print(type(x_train))
    # print(type(x_test))
    # print(type(y_train))
    # print(type(y_test))
    # print(x_train.dtype)
    # print(x_test.dtype)
    # print(y_train.dtype)
    # print(y_test.dtype)

    """ Train the model """
    train_model(x_train, y_train)

""" Load best model """
best_model = LoadBestModel()

""" Predict with the best saved model """
y_pred = best_model.predict(x_test)
print('y_pred shape:', y_pred.shape)

fig, ax = plt.subplots()
y = np.array(range(floor(np.min(y_pred)), ceil(np.max(y_pred) + 1)))
plt.plot(y, y, 'red')
ax.scatter(x=y_test[:, ], y=y_pred[:, ])
plt.xlabel('Fantasy Points scored')
plt.ylabel('Predicted Fantasy Points')
plt.title('Visual comparison for actual vs predicted values')
plt.show()

""" Calculate Error """
deviations_series = pd.Series(data=(y_test[:, ] - y_pred[:, 0]))
sns.distplot(deviations_series, bins=40)
plt.title('Error of predicted Fantasy Points values')
plt.xlabel('Error')
plt.ylabel('Frequency')
plt.show()

print('MSE:', mean_squared_error(y_test, y_pred))
print('RMSE:', mean_squared_error(y_test, y_pred, squared=False))
print('MAE:', mean_absolute_error(y_test, y_pred))
print('R^2:', r2_score(y_test, y_pred))

""" END """

