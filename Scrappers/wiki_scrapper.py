import wptools
import pandas as pd


# Open file with players heights
f = open("players_heights.csv", "w+")
# Import matches info into a dataframe
df = pd.read_csv('fantasty_challenge_matches_analytical.csv')
# Group dataframe by name
names = df.groupby('Name')['Name'].nunique().index.tolist()
# For each name extract specific attributes
for name in names:
    try:
        page = wptools.page(name).get_parse()
        infobox = page.data['infobox']
        record = name + "," + str(infobox.get('height_m')) + "\n"
        print(record)
        f.write(record)
    except:
        record = name + "," + "\n"
        print(record)
        f.write(record)

# Close open file
f.close()
