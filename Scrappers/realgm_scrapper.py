import requests
from bs4 import BeautifulSoup


# Set the URL you want to webscrape from
url1 = "https://basketball.realgm.com/international/league/1/Euroleague/stats/"
url3 = "/Averages/Qualified/All/points/All/desc/"
url5 = "/Regular_Season"

f = open("realgm_data_players_stats.csv", "w+")

for i in range(1, 21):
    url2 = 2000 + i  # iterate between 2001 to 2020
    for j in range(1, 3):
        url4 = j      # iterate between 1 and 2
        # Connect to the URL
        response = requests.get(url1 + str(url2) + url3 + str(url4) + url5)

        # Parse HTML and save to BeautifulSoup object
        soup = BeautifulSoup(response.text, "html.parser")

        # 23 arguments
        for tr in soup.find_all('tr')[1:]:
            tds = tr.find_all('td')
            f.write("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %i, %i \n"
                    % (tds[0].text, tds[1].text, tds[2].text, tds[3].text, tds[4].text, tds[5].text, tds[6].text,
                       tds[7].text, tds[8].text, tds[9].text, tds[10].text, tds[11].text, tds[12].text, tds[13].text,
                       tds[14].text, tds[15].text, tds[16].text, tds[17].text, tds[18].text, tds[19].text, tds[20].text,
                       tds[21].text, tds[22].text, (url2 - 1), url2))
