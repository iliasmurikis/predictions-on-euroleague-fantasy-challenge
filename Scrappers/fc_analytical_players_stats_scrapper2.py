import time
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver


f = open("../fantasty_challenge_matches_analytical(new).csv", "a")
flag = False
columns1 = ['RD', 'Opponent', 'Venue', 'Points']
columns2 = ['PIT', 'PTS', 'ASS', 'REB', 'BLK', 'STL', 'FLD', 'FT1M', 'FG2M',
            'FG3M', 'TO', 'BLKA', 'FOULS', 'FT1A', 'FG2A', 'FG3A', 'TW']
df1 = pd.DataFrame(columns=columns1)
df2 = pd.DataFrame(columns=columns2)

url = "https://fantasychallenge.euroleague.net/#stats-centre/player-profile/"
geckodriver_path = "/usr/local/bin/geckodriver/geckodriver"
driver = webdriver.Firefox(executable_path=geckodriver_path)

# Read the ID's from the file and add them to a list
dataset = pd.read_csv("fantasty_challenge_palyers_analytical.csv")
id_list = dataset['pageID'].tolist()

for i in id_list:
    # Create URL
    inner_url = url + str(i)
    # Fetch from URL
    driver.get(inner_url)
    time.sleep(3)
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    # First get the name of the player
    for div in soup.find_all('div', attrs={"class": "profile-name-wrapper"}):
        for player_name in div.find('p'):
            player_name = player_name.strip()

    # Create the first half
    # Initialize lists
    RDs         = []
    opponents   = []
    venues      = []
    points      = []
    new_points  = []
    # Then extract all of his attributes
    for tr in soup.find_all('tr'):
        for td in tr.find_all('td', attrs={"class": "round-id uppercase"}):
            RDs.append(td.getText().strip())
        for td in tr.find_all('td', attrs={"class": "fixture-opponent"}):
            opponents.append(td.getText().strip())
        for td in tr.find_all('td', attrs={"class": "fixture-venue"}):
            venues.append(td.getText().strip())
        for td in tr.find_all('td', attrs={"class": "uppercase"}):
            points.append(td.getText().strip())

    # Reformat points list
    for index in range(len(points)):
        if (index % 2) == 1:
            new_points.append(points[index])
    # Store to dataframe
    df1['RD']        = RDs
    df1['Opponent']  = opponents
    df1['Venue']     = venues
    df1['Points']    = new_points
    print("First part ready.")

    # Now, the rest of the dataset
    div = soup.find('div', attrs={"class": "profile-detailed-stats"})
    inner_div = div.find('div', attrs={"class": "swipe-table-outer-wrapper"})
    table = inner_div.find('table', attrs={"class": "base-table"})
    tbody = table.find('tbody')

    # Initialize list and iterations limit
    line    = []
    limit   = len(columns2)
    for tr in tbody.find_all('tr'):     # each line
        counter = 0
        for td in tr.find_all('td'):    # each cell
            counter += 1
            if counter > limit:
                break
            # Check if td's value is "-" (requires explicit casting)
            try:
                if td.getText().strip() == "-":
                    line.append(td.getText().strip())
                    continue
            except:
                pass
            line.append(((td.find('p')).getText()).strip())

        # Append each line to the dataframe
        temp_series = pd.Series(line, index=columns2)
        df2 = df2.append(temp_series, ignore_index=True)
        # Reset values
        line = []
    print("Second part ready.")

    # Concatenating the two parts
    df = pd.concat([df1, df2], axis=1).reindex(df1.index)
    # Add column with the player's name
    df['Name'] = player_name

    # with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    #     print(df)

    df.to_csv(f, mode='a', header=False, index=False)
    print("Dataframe ready.")

f.close()
driver.quit()
