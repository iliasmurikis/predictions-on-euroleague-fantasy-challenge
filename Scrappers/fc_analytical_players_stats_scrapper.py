import time
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver


f = open("fantasty_challenge_palyers_analytical(newer).csv", "a")

url = "https://fantasychallenge.euroleague.net/#stats-centre/player-profile/"
geckodriver_path = "/usr/local/bin/geckodriver/geckodriver"
driver = webdriver.Firefox(executable_path=geckodriver_path)

# Import the list of existing ids
df = pd.read_csv("~/Documents/Scholarship/Data/fantasty_challenge_palyers_analytical.csv", header=None)
id_list = df[0].tolist()

flag = False
for i in id_list[1:]:
    # Create URL
    inner_url = url + i
    # Fetch from URL
    driver.get(inner_url)
    time.sleep(3)
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    # If page has content
    # if len(soup.getText()) > 15000:
    # First get the name of the player
    for div in soup.find_all('div', attrs={"class": "profile-name-wrapper"}):
        for player_name in div.find('p'):
            record = str(i) + "," + player_name.strip()

    # Then, grouped data...
    for div in soup.find_all('div', attrs={"class": "profile-details-wrapper"}):
        # Get player's team
        for player_team in div.find('p', attrs={"class": "player-team"}):
            # Append to record
            record += "," + player_team.strip()

        # Get the playing position
        for player_pos in div.find('p', attrs={"class": "player-pos"}):
            # If contains "," replace with "&"
            player_pos = player_pos.replace(",", "&")
            # Append to record
            record += "," + player_pos.strip()

        # Get player's cost
        for player_cost in div.find('p', attrs={"class": "player-cost"}):
            # Keep only the cost
            if "m" in player_cost:
                player_cost = int(float(player_cost.split("m")[0].strip()[2:]) * 1000)
            else:
                player_cost = player_cost.split("k")[0].strip()[2:]

            # Append to record
            record += "," + str(player_cost)

    # Then extract all of his attributes
    counter = 0
    for tr in soup.find_all('tr'):
        for td in tr.find_all('td'):
            counter += 1
            if counter <= 12:
                for p in td.find('p', attrs={"class": "pl-stat-data"}):     # label or data
                    record += "," + str(p.strip())
            else:
                flag = True
                record += "\n"
                f.write(record)
                record = ''
                print("Player %s addded." % player_name.strip())
                break
        if flag:
            flag = not flag     # False
            break
    # else:
    #     print("Number %i does not corresponds to page." % i)
    #     continue

f.close()
driver.quit()
