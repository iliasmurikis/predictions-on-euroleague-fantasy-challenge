import time
from bs4 import BeautifulSoup
from selenium import webdriver


f = open("fc_fantastic_teams_data.csv", "a")

url = "https://fantasychallenge.euroleague.net/#login"
geckodriver_path = "/usr/local/bin/geckodriver/geckodriver"
driver = webdriver.Firefox(executable_path=geckodriver_path)

try:
    driver.get(url)
    time.sleep(3)
    # Press Sign Up
    driver.find_element_by_id("js-auth-button").click()
    time.sleep(3)
    # Entering credentials
    driver.find_element_by_name("_username").send_keys("euroleague@plusprivacy.com")
    driver.find_element_by_name("_password").send_keys("f23LeZ5r9ukUiqj")
    driver.find_element_by_id("m_login_signin_submit").click()
    time.sleep(5)
    # Go to rankings page
    stats = driver.find_elements_by_xpath('//*[@href="#classic/rankings"]')

    stats[1].click()
    # """ Flag is used to ignore the first element that
    #     corresponds to the mobile version of the site   """
    # flag = False
    # for s in stats:
    #     if flag:
    #         s.click()
    #     flag = True

    time.sleep(3)

    try:
        # Click until the button is no more available
        while 1:
            # Find and press the button to load more imaginary teams
            driver.find_element_by_xpath('//a[@class="js-load-more btn leagues-more-btn"]').click()
            time.sleep(1)
    except Exception as e:
        print(e)

    # Find all the buttons from imaginary team
    for i in driver.find_elements_by_xpath('//*[@class="fa fa-chevron-right"]'):
        # Click the button to see the details of the player
        i.click()

        """ Scrap the page """
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        # Get team's owner name
        header = soup.find('div', attrs={'class': 'game-field-heading'})
        div = header.find('div', attrs={'class': 'team-name'})
        span = div.find('span')
        owner_name = span.getText().strip()

        # Get a list with the team's players
        team_players = []
        for a in soup.find_all('a', attrs={'href': 'javascript:void(0);'}):
            team_players.append(a.getText().strip())

        team_players = team_players[8:]

        # Create record to be stored
        record = owner_name
        for name in team_players:
            record += ","
            record += name
        record += "\n"
        # Append the data into a file
        f.write(record)

        time.sleep(1)

        # Press the back button and return to the main list
        back_button = driver.find_element_by_xpath("//div[@class='back-btn js-back']")
        back_button.click()

except Exception as e:
    print(e)
finally:
    f.close()
    driver.quit()
