import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import accuracy_score, confusion_matrix

df = pd.read_csv('../Data/scores.csv')
actual = df['actual_win'].to_numpy()
predicted = df['win'].to_numpy()
print(actual)
print(predicted)

""" Printing accuracy """
print(accuracy_score(y_true=actual, y_pred=predicted))

""" Plot confusion matrix """
cm = confusion_matrix(y_true=actual, y_pred=predicted)
# print(cm)
sns.heatmap(cm, annot=True)
plt.show()
