import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import load_model


def trajectories_to_supervised_fast_df(data):
    df_shifted__1 = data.copy()
    df_shifted__1['cumcount__1'] = df_shifted__1.groupby(['PlayerID']).cumcount(ascending=False)
    df_shifted__1.drop(df_shifted__1[df_shifted__1['cumcount__1'] == 0].index, inplace=True)
    df_shifted__1.drop(columns=['cumcount__1'], inplace=True)
    df_shifted__1.reset_index(drop=True, inplace=True)
    df_shifted__1.rename(columns=lambda x: ('%s(t-1)' % x), inplace=True)
    df_shifted_1 = data.copy()
    df_shifted_1['cumcount_1'] = df_shifted_1.groupby(['PlayerID']).cumcount()
    df_shifted_1.drop(df_shifted_1[df_shifted_1['cumcount_1'] == 0].index, inplace=True)
    df_shifted_1.drop(columns=['cumcount_1'], inplace=True)
    df_shifted_1.reset_index(drop=True, inplace=True)
    df_shifted_1.rename(columns=lambda x: ('%s(t)' % x), inplace=True)
    df0 = pd.concat([df_shifted__1, df_shifted_1], axis=1)

    return df0


def ColumnRemoverFrom3DArray(data, excluded_column):
    x = np.zeros((data.shape[0], data.shape[1], data.shape[2] - 1))
    x_counter = 0
    for i in range(0, data.shape[2]):
        if not i == excluded_column:
            x[:, :, x_counter] = data[:, :, i]
            x_counter += 1

    return x


def LoadBestModel():
    path = "../Models/ShotsPrediction/approachSimpleRNN.hdf5"
    if os.path.exists(path):
        model = load_model(path)
        return model
    else:
        print("Warning: Best model does not exist!")
        return 0


def show_reduction(old, new):
    text = '|' + str(old) + '->' + str(new) + '|'
    text_length = len(text)
    print(text_length * '-')
    print(text)
    print(text_length * '-')


# Packages settings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.options.mode.chained_assignment = None  # default='warn'

# Program begins...
df = pd.read_csv("../Data/main_dataset.csv")
matches_df = pd.read_csv('../Data/last_played_matches_dataset_IDs.csv')

# Order by PlayerID and then by RD
df = df.sort_values(by=['PlayerID', 'RD'])

# Replace non-numerical values with NaN
df = df.replace('-', np.NaN)
df = df.replace('DNP', np.NaN)
# Drop nan values or replace
df = df.dropna()
df.reset_index(drop=True, inplace=True)
# Drop unnecessary columns
df = df.drop(['RD', 'PlayerName', 'Venue'], axis=1)
# print(df.shape)
# print(df.columns.values)
# print(df.describe())
# print(df.head(5))

"""--- Convert series to supervised problem ---"""
STEP = 5
list_of_xs = []
list_of_ys = []
grouped = df.groupby('PlayerID')
# print(len(grouped))
# 269 players into the cleared dataset
for _, group in grouped:
    index = 0
    for global_index, row in group.iterrows():
        # Creating a local index, due to the mistaken global that pandas uses
        """ Checking if there are at least 2 previous matches,
            so with the current on to be STEP, stateful=False """
        if (index + STEP) > len(group) or len(group) < STEP:
            index = 0
            break

        """ Create steps """
        # Here, global_index is used because it was reset after the sorting
        steps = group.loc[global_index:global_index + STEP - 1].to_numpy()
        # print(steps)
        # print(steps.shape)
        list_of_xs.append(steps)

        """ Extract the to-be-predicted column """
        y = steps[STEP - 1, 7]
        # print(y)
        list_of_ys.append(y)

        # Move to the next position
        index += 1
# print(len(list_of_xs))

""" Convert lists into arrays """
x = np.zeros((len(list_of_xs), list_of_xs[0].shape[0], list_of_xs[0].shape[1]))

for i in range(len(list_of_xs)):
    x[i, :, :] = list_of_xs[i]
# print(x.shape)
# print(type(x))
y = np.asarray(list_of_ys)
# print(y)
# print(y.shape)

""" Set specific values equal to 0 in the first/un-played match """
for i in range(len(list_of_xs)):
    x[i, STEP - 1, 5:23] = 0
print("x shape:", x.shape)
print("y shape:", y.shape)

""" Normalise data inside [0, 1], using StandardScaler """
# Convert 3D array to 2D
players, matches, attrs = x.shape
x = x.reshape((players * matches, attrs))
# Apply normalization
sc_X = StandardScaler()
x = sc_X.fit_transform(x)
# Convert 2D array to 3D
x = x.reshape((players, matches, attrs))

""" Load best model """
best_model = LoadBestModel()

""" Predict with the best saved model """
y_pred = best_model.predict(x)

""" Inverse transform normalisation """
# Convert x back to 2-dimensional
x = x.reshape((players * matches, attrs))
# Inverse transform normalized values
x = sc_X.inverse_transform(x)
# And again to 3-dimensional
x = x.reshape((players, matches, attrs))

""" Create lists with the new columns names """
columns_list = ['PlayerID', 'Height', 'Cost', 'Latitude', 'Longtitude', 'FantasyPoints', 'PIT',
                'PTS', 'ASS', 'REB', 'BLK', 'STL', 'FLD', 'FT1M', 'FG2M', 'FG3M', 'TO', 'BLKA',
                'FOULS', 'FT1A', 'FG2A', 'FG3A', 'TW', 'TeamID', 'OpponentTeamID', 'PositionID']
columns_list_new = ['PlayerID', 'Cost', 'TeamID', 'OpponentTeamID', 'PositionID']

""" Merging x, y into one df """
part1 = pd.DataFrame(data=x[:, -1, :], columns=columns_list)
# print(part1.head(10))
part2 = pd.DataFrame(data=y, columns=['PTS_pred'])
# print(part2.head(10))
# sns.distplot(part2)
# plt.show()
new_df = part1.join(part2)
# print(new_df.head(5))
# Round df columns due to numpy to pandas conversion
new_df[columns_list_new] = new_df[columns_list_new].round()
# print(new_df.head(5))
for item in columns_list:
    new_df[item] = pd.to_numeric(new_df[item])
# print(new_df.head(5))

""" Adding prediction to the initial dataframe (df) """
for index, row in new_df.iterrows():
    df.loc[(df['TeamID'] == row['TeamID']) &
           (df['OpponentTeamID'] == row['OpponentTeamID']) &
           (df['PlayerID'] == row['PlayerID']), 'PTS_pred'] = new_df.iloc[index]['PTS_pred']
# print(df.head(5))

""" Drop rows where 'PTS_pred' is NaN """
before = len(df)
df = df.dropna()
show_reduction(before, len(df))
# sns.distplot(df['PTS_pred'])
# plt.show()

""" Create a new dataframe where predicted scores for 
    each match are stored                             """
matches_df = matches_df.drop(['Unnamed: 0'], axis=1)
# print(matches_df.head())
# print(matches_df.columns.values)
scores_df = matches_df.copy()
# print(scores_df.columns.values)
# print(matches_df.head())
scores_df.iloc[:, 2:] = -1
# print(scores_df.head(10))

""" Now, pass the predicted shots in the new scores_df,
    by aligning the data in df0 and matches_df          """
# Iterating through each team's player names
string_name = 'Team_player'
string_name2 = 'Opponent_player'
for row_index, _ in matches_df.iterrows():
    # First team
    try:
        for column in range(1, 11):
            players_id = int(matches_df.iloc[row_index][string_name + str(column) + 'ID'])
            scores_df.at[row_index, string_name + str(column) + 'ID'] = int(df.loc[
                                                (df['TeamID'] == matches_df.iloc[row_index]['TeamID']) &
                                                (df['OpponentTeamID'] == matches_df.iloc[row_index]['OpponentID']) &
                                                (df['PlayerID'] == players_id), 'PTS_pred'].values[-1])
    except Exception as e:
        pass
    # Opponent team
    try:
        for column2 in range(1, 11):
            players_id2 = int(matches_df.iloc[row_index][string_name2 + str(column2) + 'ID'])
            scores_df.at[row_index, string_name2 + str(column2) + 'ID'] = int(df.loc[
                                                  (df['OpponentTeamID'] == matches_df.iloc[row_index]['TeamID']) &
                                                  (df['TeamID'] == matches_df.iloc[row_index]['OpponentID']) &
                                                  (df['PlayerID'] == players_id2), 'PTS_pred'].values[-1])
    except Exception as e:
        pass
# print(scores_df.head(50))

""" Change scores_df column names """
original_columns_list = list(scores_df.columns.values)
columns_list3 = ['TeamID', 'OpponentID']
for i in range(1, 11):
    columns_list3.append(string_name  + str(i) + 'Shots')
    columns_list3.append(string_name2 + str(i) + 'Shots')
# print(columns_list3)
# print(scores_df.head())
# Convert lists into dictionary
dictionary = dict(zip(original_columns_list, columns_list3))
# print(dictionary)
scores_df = scores_df.rename(columns=dictionary)
# print(scores_df.head())

""" Replace -1 values with NaN """
# before = len(scores_df)
scores_df = scores_df.replace(-1, np.nan)
# print(scores_df[scores_df == -1].sum())
# Drop nan values or replace
# scores_df = scores_df.dropna()
scores_df.reset_index(drop=True, inplace=True)
# show_reduction(before, len(scores_df))
# print(scores_df.head(50))
# print(scores_df.dtypes)

""" Initiate three new columns """
scores_df['team1'] = -1
scores_df['team2'] = -1
scores_df['win'] = -1

""" Sum-up each teams score """
for row_index, _ in scores_df.iterrows():
    team_sum = 0
    opponent_sum = 0
    for column in range(1, 11):
        if not np.isnan(scores_df.iloc[row_index][string_name + str(column) + 'Shots']):
            team_sum += scores_df.iloc[row_index][string_name + str(column) + 'Shots']

        if not np.isnan(scores_df.iloc[row_index][string_name2 + str(column) + 'Shots']):
            opponent_sum += scores_df.iloc[row_index][string_name2 + str(column) + 'Shots']

    scores_df['team1'].iloc[row_index] = team_sum
    scores_df['team2'].iloc[row_index] = opponent_sum
    scores_df['win'].iloc[row_index] = 1 if team_sum > opponent_sum else 2 if team_sum < opponent_sum else 0

""" Removing records with score 0-0 """
indexNames = scores_df[(scores_df['team1'] == 0) & (scores_df['team2'] == 0)].index
scores_df.drop(indexNames, inplace=True)

print(scores_df['win'].value_counts())
# scores_df = scores_df.dropna()
# scores_df.reset_index(drop=True, inplace=True)
print(scores_df[['team1', 'team2', 'win']])

""" Delete duplicate rows """
scores_df = scores_df.drop_duplicates()

""" Add one empty column for actual winning """
scores_df['actual_win'] = -1

# Exporting to csv...
scores_df.to_csv('../Data/scores.csv')
