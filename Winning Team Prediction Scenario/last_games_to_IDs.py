import pandas as pd


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 1000)

df = pd.read_csv('../Data/last_played_matches_dataset.csv')

players_df = pd.read_csv('../Data/players.csv')
teams_df = pd.read_csv('../Data/distinct_teams_IDs.csv')

# Map teams IDs
df['TeamID'] = df['Team'].map(teams_df.set_index('Team')['ID'])
df['OpponentID'] = df['Opponent'].map(teams_df.set_index('Team')['ID'])

# Map players id
for i in range(1, 11):
    df['Team_player'+str(i)+'ID'] = df['Team_player'+str(i)].map(players_df.set_index('PlayerName')['playerID'])
    df['Opponent_player'+str(i)+'ID'] = df['Opponent_player'+str(i)].map(players_df.set_index('PlayerName')['playerID'])

# print(df.columns.values)
df = df.drop(['Team', 'Opponent', 'Team_player1', 'Team_player2', 'Team_player3',
              'Team_player4', 'Team_player5', 'Team_player6', 'Team_player7',
              'Team_player8', 'Team_player9', 'Team_player10', 'Opponent_player1',
              'Opponent_player2', 'Opponent_player3', 'Opponent_player4',
              'Opponent_player5', 'Opponent_player6', 'Opponent_player7',
              'Opponent_player8', 'Opponent_player9', 'Opponent_player10'], axis=1)

print(df.head())
df.to_csv('Data/last_played_matches_dataset_IDs.csv')
